# comptoir

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with comptoir](#setup)
    * [What comptoir affects](#what-comptoir-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with comptoir](#beginning-with-comptoir)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

Install and configure all requirements for *Comptoir du Libre* application.

## Setup

### What comptoir affects

This release of module comptoir permits to :

  * configure PPA
  * install Deb packages required

### Setup Requirements

This module needs `puppetlabs-stdlib` module.

### Beginning with comptoir

Here use case of functionnal hiera data :
```
---

comptoir::ppa_defs:
  postgresql:
    keybits: '4096R'
    keyid: 'ACCC4CF8'
    location: 'http://apt.postgresql.org/pub/repos/apt/'
    release: 'trusty-pgdg'
    repos:
      - 'main'

comptoir::cdlpackages:
  - libicu-dev
  - libicu52
  - zlib1g-dev
  - git
  - apache2
  - php5
  - php5-intl
  - libapache2-mod-php5
  - postgresql-9.5
  - php5-pgsql

```

## Usage

This section is where you describe how to customize, configure, and do the
fancy stuff with your module here. It's especially helpful if you include usage
examples and code samples for doing things with your module.

## Reference

### classes

#### `comptoir`

  * $ppa_defs (hash) : define the PPA to be configured on a fresh install.
  * $cdlpackages (array) : list packages to be installed on a fresh install.

### defined resources

#### `comptoir::configure::ppa`

  * $ensure (string) : accepted values < present | absent > ( * default : present * )
  * $keybits (string) : key bits used by the ppa key
  * $keyid (string> : key id used by the ppa ,
  * $location (string) : url where packages are downloaded (<http://> or <ftp://)>,
  * $release (string) : os release defined in the ppa,
  * $repos (array) : repositories defined in the ppa,

## Limitations

This module is only available for `Ubuntu-14.04`. It raise a `fail` if you run on other OS.

## Development

Git repository available here : <https://gitlab.adullact.net/Comptoir/Comptoir-puppet>

## Release Notes/Contributors/Etc.

* release 0.1 : initial release.

