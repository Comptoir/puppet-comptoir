# Generals ideas about `puppet apply`

Puppet apply is an application that compiles and manages configurations on nodes. It acts like a self-contained combination of the Puppet master and Puppet agent applications. Unlike Puppet agent, Puppet apply never runs as a daemon or service. It always runs as a single task in the foreground, which compiles a catalog, applies it, files a report, and exits. By default, it never initiates outbound network connections, although it can be configured to do so. It never accepts inbound network connections. Puppet can run in a stand-alone architecture, where each managed server has its own complete copy of your configuration info and compiles its own catalog.

## 1. install puppet binary

This binary is provided by the .deb package named `puppet-agent`.

```sh
wget https://apt.puppetlabs.com/puppetlabs-release-pc1-trusty.deb
sudo dpkg -i puppetlabs-release-pc1-trusty.deb
sudo apt-get update
sudo apt-get -y install puppet-agent
```

## 2. Create a directory structure

```shell
mkdir -p /tmp/comptoir/modules
mkdir -p /tmp/comptoir/hieradata
```

## 3. Fill in `modules` directory

You have to put in the directory `/tmp/comptoir/modules` all modules needed for your `puppet apply` to run. To do so:

1. Copy all the content of this very repository (*comptoir*) in `/tmp/comptoir/modules`
1. Copy the Puppet std-libs:
   ```
   sudo /opt/puppetlabs/puppet/bin/puppet module install puppetlabs-stdlib
   cp -ar /etc/puppetlabs/code/environments/production/modules/stdlib /tmp/comptoir/modules/
   ```

## 4. Configure Hiera

First create file `/tmp/comptoir/hiera.yaml` with:

```
---
:backends:
  - yaml
:hierarchy:
  - defaults

:yaml:
  :datadir: /tmp/comptoir/hieradata
```

Then

```sh
cp /tmp/comptoir/modules/comptoir/hieradata/defaults.yaml /tmp/comptoir/hieradata/
```

## 5. Run Puppet in "apply" mode

This module needs to install .deb packages, so using sudo is needed:

```
sudo /opt/puppetlabs/bin/puppet apply --hiera_config=/tmp/comptoir/hiera.yaml --modulepath=/tmp/comptoir/modules -e 'include comptoir'
```


