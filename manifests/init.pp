# Install and configure Comptoir Du Libre
#
# @summary Install and configure Comptoir Du Libre
#
# @param comptoir_db_password Password for the database user
# @param system_user Username of Unix account holding Comptoir du Libre application
# @param refspec Revision of Comptoir repos as understood by module VCSRepos, may be a branch, a tag, a SHA-1
# @param comptoir_db_user Database username owning the database for the application
# @param comptoir_db_name Database name to be used for the application
# @param comptoir_db_host Name of the database host, must an FQDN or an IP address
# @param comptoir_db_port Port of the database host, must an integer
# @param debug Install Comptoir in dev mode with debug activated
#
# @example
#   include comptoir
class comptoir (
  # Mandatory parameter: DB password
  String $comptoir_db_password,

  # Comptoir app parameters
  String $system_user          = 'comptoir',
  String $refspec              = 'master',
  Boolean $debug               = false,

  # DB parameters
  String $comptoir_db_host     = 'localhost',
  String $comptoir_db_port     = '5432',
  String $comptoir_db_username = 'comptoir',
  String $comptoir_db_name     = 'comptoir',

  # Apache parameters
  String $apache_user          = 'www-data',
) {
  # #############################################################################
  # Variables

  $comptoir_repos_url = 'https://gitlab.adullact.net/Comptoir/Comptoir-srv.git'

  $_comptoir_dir = join([ '/home/', $system_user, '/', basename($comptoir_repos_url, '.git') ])
  $_sql_create_tables_and_procedure = 'config/SQL/COMPTOIR_DB_create_tables_and_procedures.sql'

  $_file_app_php = "${_comptoir_dir}/config/app.php"
  $_file_comptoir_php = "${_comptoir_dir}/config/comptoir.php"

  $_psql_create_table_cmd = "\\i ${_comptoir_dir}/${_sql_create_tables_and_procedure}"
  $_my_sed = '/bin/sed -i -e'
  $_sed_barman_dev =
    "\"s!'from' => 'barman@comptoir-du-libre.org'!'from' => 'barman-DEV@comptoir-du-libre.org'!\""

  # Grab source code
  vcsrepo { $_comptoir_dir:
    ensure   => present,
    provider => git,
    source   => $comptoir_repos_url,
    revision => $refspec,
    user     => $system_user,
    force    => true, # See https://forge.puppet.com/puppetlabs/vcsrepo#force
  }

  # Create SQL tables & procedures
  # /!\ This is NOT idempotent => improve it :)
  # TODO Improve puppet code to be idempotent:
  # TODO 1. by creating an idempotent SQL, see https://stackoverflow.com/questions/33631186/how-do-i-make-alter-column-idempotent
  # TODO 2. by adding a directive unless or onlyif

  postgresql_psql { 'Create SQL tables & procedures':
    command   => $_psql_create_table_cmd,
    db        => $comptoir_db_name,
    psql_user => $comptoir_db_username,
  }

  # Install app with Composer
  -> exec { 'composer install':
    command     => '/usr/local/bin/composer --no-progress install',
    cwd         => $_comptoir_dir,
    user        => $system_user,
    environment => [ 'HOME=/home/comptoir' ],
    timeout     => 1200, # default value (300s) * 4
  }

  # Comptoir configuration: APP.PHP --> debug mode
  if $debug {
    exec { 'Comptoir APP.PHP enable debug':
      command     => "${_my_sed}  's!//COMPTOIR-DEBUG!!' ${_file_app_php}",
      cwd         => $_comptoir_dir,
      user        => $system_user,
      environment => [ 'HOME=/home/comptoir' ],
      onlyif      => "/bin/grep '//COMPTOIR-DEBUG' ${_file_app_php}",
      require     => Exec['composer install'],
    }
    -> exec { 'Comptoir APP.PHP barman-DEV':
      command     => "${_my_sed} ${_sed_barman_dev} ${_file_app_php}",
      cwd         => $_comptoir_dir,
      user        => $system_user,
      environment => [ 'HOME=/home/comptoir' ],
      unless      => "/bin/grep 'barman-DEV' ${_file_app_php}",
    }
  }

  # Comptoir configuration: APP.PHP
  exec { 'Comptoir APP.PHP salt':
    command     => "${_my_sed}  's/__SALT__/somerandomsalt/' ${_file_app_php}",
    cwd         => $_comptoir_dir,
    user        => $system_user,
    environment => [ 'HOME=/home/comptoir' ],
    onlyif      => "/bin/grep '__SALT__' ${_file_app_php}",
    require     => Exec['composer install'],
  }
  -> exec { 'Comptoir APP.PHP Session default':
    command => "${_my_sed} \"s!'php',!env('SESSION_DEFAULTS', 'php'), !\" ${_file_app_php}",
    cwd     => $_comptoir_dir,
    user    => $system_user,
    unless  => "/bin/grep 'SESSION_DEFAULTS' ${_file_app_php}",
  }

  # Comptoir configuration: COMPTOIR.PHP
  file { 'Comptoir COMPTOIR.PHP creation':
    ensure  => present,
    path    => $_file_comptoir_php,
    content => template('comptoir/comptoir.php.epp'),
    owner   => $system_user,
    require => Exec['composer install'],
  }

  # Set owner + group + permissions on directories tmp/ logs/

  $_cake_dirs = [ "${_comptoir_dir}/tmp", "${_comptoir_dir}/logs" ]

  file { $_cake_dirs:
    ensure  => directory,
    owner   => $system_user,
    group   => $apache_user,
    mode    => '0775',
    recurse => true,
    require => Exec['composer install'],
  }

  $_cake_dirs.each |$item| {
    exec { "chmod -R o-w ${item}":
      command => "/bin/chmod -R o-w ${item}",
      cwd     => $_comptoir_dir,
      user    => $system_user,
      onlyif  => "/usr/bin/find ${item} -maxdepth 0 -perm -o=w",
      require => Exec['composer install'],
    }
  }

}
