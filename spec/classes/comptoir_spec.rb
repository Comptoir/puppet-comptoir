require 'spec_helper'

describe 'comptoir' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do
        {
          comptoir_db_password: 'kiki34',
        }
      end

      it { is_expected.to compile }
    end
  end
end
